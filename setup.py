# -*- coding: utf-8 -*-

from setuptools import setup

with open('README.md') as f:
    readme = f.read()

setup(
    name='systraynotes',
    version='0.1.0',
    description='Simple systray icon that lists and watch text files, and open in your favorite editor',
    long_description=readme,
    author='Felipe Ramos Ferreira',
    author_email='perenecabuto@gmail.com',
    license='BSD',
    url='...',
    scripts=['bin/systraynotes'],
    include_package_data=True,
    package_data={'': ['systraynotes/systraynotes.png']},
    packages=['systraynotes'],
    classifiers=[
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: Linux',
        'Programming Language :: Python',
    ],
    zip_safe=False,
)

