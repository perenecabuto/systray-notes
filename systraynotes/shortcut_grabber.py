# -*- coding: utf-8 -*-

import threading

from Xlib import X, XK
from Xlib.display import Display


class ShortcutGrabber(object):
    shortcuts = {}

    def __init__(self):
        self.display = Display()
        self.root = self.display.screen().root

        self.root.change_attributes(event_mask=X.KeyReleaseMask)

    def add_shortcut(self, keyname, callback):
        keysym = XK.string_to_keysym(keyname)
        keycode = self.display.keysym_to_keycode(keysym)

        self.shortcuts[keycode] = callback
        self.root.grab_key(keycode, X.AnyModifier, False, X.GrabModeSync, X.GrabModeAsync)

    def listen_events(self):
        while True:
            event = self.display.next_event()
            event_keycode = event.detail

            if event.type == X.KeyReleaseMask and event_keycode in self.shortcuts:
                callback = self.shortcuts[event_keycode]
                callback()

    def start(self):
        t = threading.Thread(target=self.listen_events)
        t.daemon = True
        t.start()
