#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import signal

from watchdog.observers import Observer
from shortcut_grabber import ShortcutGrabber

from PyQt4.QtGui import QApplication
from PyQt4.QtCore import SIGNAL

from components import SystemTrayIcon
from watcher import SystrayNotesWatchdogHandler


DEFAULT_EDITOR = '/bin/false'
DEFAULT_DIR_PATH = '/tmp'
DEFAULT_VALID_EXTENSIONS = ('.txt', '.md', '.mkd', '.rst')
ICON_IMG = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'systraynotes.png')


def main():
    notes_dir_path = sys.argv[1] if len(sys.argv) > 1 else '.'
    editor_bin_path = sys.argv[2] if len(sys.argv) > 1 else DEFAULT_EDITOR

    app = QApplication(sys.argv)
    icon = SystemTrayIcon(
        notes_dir_path=notes_dir_path, editor_bin_path=editor_bin_path,
        icon_img=ICON_IMG, invalid_extensions=DEFAULT_VALID_EXTENSIONS
    )
    icon.app = app
    event_handler = SystrayNotesWatchdogHandler(icon)
    shortcut_grabber = ShortcutGrabber()
    observer = Observer()

    #logging.basicConfig(level=logging.DEBUG)
    shortcut_grabber.add_shortcut('F12', lambda: icon.systray.emit(SIGNAL('toggle_menu()')))
    shortcut_grabber.start()
    observer.schedule(event_handler, path=notes_dir_path, recursive=True)
    observer.start()

    signal.signal(signal.SIGINT, signal.SIG_DFL)
    sys.exit(app.exec_())
    observer.stop()
    observer.join()


if __name__ == '__main__':
    main()

