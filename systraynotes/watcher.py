# -*- coding: utf-8 -*-

import os

from PyQt4.QtCore import SIGNAL
from watchdog.events import FileSystemEventHandler


class SystrayNotesWatchdogHandler(FileSystemEventHandler):

    def __init__(self, systray):
        super(SystrayNotesWatchdogHandler, self).__init__()
        self.systray = systray

    def on_any_event(self, event):
        super(SystrayNotesWatchdogHandler, self).on_any_event(event)
        changed_path = os.path.dirname(event.src_path)
        current_path = os.path.dirname(self.systray.current_path)

        signal = SIGNAL('notify_change(QString, QString)')

        deleted = event.event_type == 'moved' or event.event_type == 'deleted' or not os.path.exists(event.src_path)
        delta = os.stat(event.src_path).st_mtime - os.stat(event.src_path).st_atime if not deleted else 0
        just_created = event.event_type == 'created' and delta <= 1
        modified_a_long_time = event.event_type == 'modified' and delta > 1

        if deleted or just_created or modified_a_long_time:
            self.systray.emit(signal, event.src_path, event.event_type)

        if changed_path == current_path:
            self.systray.emit(SIGNAL('sync()'))

