# -*- coding: utf-8 -*-

import os
import logging
import math

from PyQt4 import QtGui, QtCore

DEFAULT_LABEL = 'Systray Notes'


class SystemTrayIcon(object):

    def __init__(self, notes_dir_path, editor_bin_path, icon_img='', invalid_extensions=[]):
        self.icon_img = icon_img
        self.invalid_extensions = invalid_extensions
        self.editor_bin_path = editor_bin_path
        self.current_path = self.notes_dir_path = notes_dir_path
        self.last_changed_note_path = None

        self.update_position()
        self.assign_events()
        self.build_menu()
        self.sync()

        self.show()
        self.hide_menu()

    @property
    def systray(self):
        if not getattr(self, '_systray', None):
            parent = QtGui.QWidget()
            parent.setWindowFlags(QtCore.Qt.CustomizeWindowHint | QtCore.Qt.WindowStaysOnTopHint)
            parent.setAttribute(QtCore.Qt.WA_TranslucentBackground, True)
            menu, icon = QtGui.QMenu(parent), QtGui.QIcon(self.icon_img)
            menu.hideEvent = lambda e: parent.hide()

            self._systray = QtGui.QSystemTrayIcon(icon, parent)
            self._systray.menu = menu
            self._systray.setContextMenu(menu)
            self._systray.parent = parent

        return self._systray

    @property
    def menu(self):
        return self.systray.menu

    @property
    def title(self):
        return self.current_path.replace(self.notes_dir_path, '')[:-1] or DEFAULT_LABEL

    def assign_events(self):
        QtCore.QObject.connect(self.systray, QtCore.SIGNAL('activated(QSystemTrayIcon::ActivationReason)'), self.bind_click)
        QtCore.QObject.connect(self.systray, QtCore.SIGNAL('messageClicked()'), self.message_clicked)

        QtCore.QObject.connect(self.systray, QtCore.SIGNAL('notify_change(QString, QString)'), self.notify_change)
        QtCore.QObject.connect(self.systray, QtCore.SIGNAL('toggle_menu()'), self.toggle_menu)
        QtCore.QObject.connect(self.systray, QtCore.SIGNAL('sync()'), self.sync)

        QtCore.QObject.connect(self.systray, QtCore.SIGNAL('add_note(QString)'), self.add_note)
        QtCore.QObject.connect(self.systray, QtCore.SIGNAL('remove_note(QString)'), self.remove_note)
        QtCore.QObject.connect(self.systray, QtCore.SIGNAL('add_notebook(QString)'), self.add_notebook)
        QtCore.QObject.connect(self.systray, QtCore.SIGNAL('remove_notebook(QString)'), self.sync)

    def message_clicked(self):
        if self.get_mouse_distance() > 20 and os.path.exists(self.last_changed_note_path):
            self.open_editor(self.last_changed_note_path)

    def bind_click(self, reason):
        if QtGui.QSystemTrayIcon.Trigger == reason:
            mouse_pos = self.get_mouse_pos()
            return self.menu.popup(mouse_pos)

        self.hide_menu()

    def notify_change(self, path, text):
        if self.is_valid_note_path(path):
            label = self.get_label(path)
            self.last_changed_note_path = path
            self.show_message(self.title, '{0} {1}'.format(text, label))

    def show_message(self, label, text):
        self.systray.showMessage(label, text, 1, 2000)
        self.systray.show()

    def build_menu(self):
        self.show_title()
        self.add_action('new note', lambda: self.open_editor())
        self.add_action('exit', QtGui.qApp.quit)
        self.add_separator()

    def rebuild_menu(self):
        if hasattr(self, 'menu'):
            self.menu.clear()

        return self.build_menu()

    def emit(self, signal, *args, **kwargs):
        self.systray.emit(signal, *args, **kwargs)

    def show(self):
        self.systray.show()
        self.menu.show()

    def show_menu(self):
        point = self.get_menu_position_point()
        self.systray.parent.show()
        self.menu.popup(point)

    def get_menu_position_point(self):
        pos = self.systray.parent.pos()
        return QtCore.QPoint(pos.x(), pos.y())

    def hide_menu(self):
        self.systray.parent.hide()
        self.menu.hide()

    def toggle_menu(self):
        if self.menu.isHidden():
            self.show_menu()
        else:
            self.hide_menu()

    def show_title(self):
        action = self.add_action(self.title)
        font = action.font()
        font.setBold(True)
        action.setFont(font)

        self.add_separator()

    def add_separator(self):
        self.menu.addSeparator()

    def add_action(self, label, trigger_callback=None, action_id=None):
        logging.debug('Adding {0} action'.format(label))
        btn = self.menu.addAction(label)

        if trigger_callback:
            btn.triggered.connect(trigger_callback)

        return btn

    def remove_action(self, *args, **kwargs):
        self.menu.removeAction(*args, **kwargs)

    def update_position(self):
        # TODO discover its real position
        x, y = 10, 10
        # Set main window position witch is necessary to grap keyboard actions
        self.systray.parent.setGeometry(x, y, x, y)

    def get_mouse_distance(self):
        mouse_pos = self.get_mouse_pos()
        mx, my = mouse_pos.x(), mouse_pos.y()
        px, py = self.systray.parent.pos().x(), self.systray.parent.pos().y()

        return math.sqrt(((mx - px) ** 2) + ((my - py) ** 2))

    def get_mouse_pos(self):
        return QtGui.QCursor.pos()

    def set_current_path(self, path):
        self.current_path = os.path.join(os.path.realpath(path), '')
        self.sync()
        self.show()

    def is_valid_note_path(self, path):
        if isinstance(path, QtCore.QString):
            path = str(path)

        label = self.get_label(path)

        return any(
            not label.startswith('.') and label.endswith(ve)
            for ve in self.invalid_extensions
        )

    def get_label(self, path):
        return str(path).replace(self.current_path, '')

    def add_note(self, path):
        if not self.is_valid_note_path(path):
            return

        label = self.get_label(path)
        self.add_action(label, lambda: self.open_editor(path))

    def remove_note(self, path):
        label = self.get_label(path)

        for action in self.menu.actions():
            if str(action.text()) == label:
                self.remove_action(action)

    def add_notebook(self, path):
        label = self.get_label(path)

        if label.startswith('.') and not label.startswith('..'):
            return

        self.add_action(label, lambda: self.set_current_path(path))

    def remove_notebook(self, path):
        label = self.get_label(path)

        for action in self.menu.actions():
            if str(action.text()) == label:
                self.menu.removeAction(action)

    def open_editor(self, path=''):
        QtCore.QProcess.startDetached(self.editor_bin_path, [path], self.notes_dir_path)

    def sync(self):
        self.rebuild_menu()

        if self.current_path != self.notes_dir_path:
            self.add_notebook(os.path.join(self.current_path, '..'))

        for root, dirs, files in os.walk(self.current_path):
            if root != self.current_path:
                continue

            mtime = lambda f: os.stat(os.path.join(root, f)).st_mtime

            for fname in reversed(sorted(dirs, key=mtime)):
                path = os.path.join(root, fname, '')
                self.add_notebook(path)

            self.add_separator()

            for fname in reversed(sorted(files, key=mtime)):
                path = os.path.join(root, fname)

                try:
                    self.add_note(path)
                except InvalidNotePathError:
                    logging.warn('Ignoring: {0}'.format(path))
                    pass


class InvalidNotePathError(Exception):
    pass

