# -*- coding: utf-8 -*-

import threading

from Xlib import X, XK
from Xlib.display import Display


def grab_keyname(root, display, n):
    keysym = XK.string_to_keysym(n)
    keycode = display.keysym_to_keycode(keysym)
    root.grab_key(keycode, X.AnyModifier, False, X.GrabModeSync, X.GrabModeAsync)


def main():
    # current display
    display = Display()
    root = display.screen().root
    root.change_attributes(event_mask=X.KeyPressMask | X.KeyReleaseMask)

    grab_keyname(root, display, "F12")

    #signal.signal(signal.SIGALRM, lambda a,b:sys.exit(1))
    #signal.alarm(4)

    def listen_events(d):
        while True:
            event = d.next_event()
            print event.type

    t = threading.Thread(target=listen_events, args=(display,))
    t.daemon = True
    t.start()

    while True:
        pass

main()
