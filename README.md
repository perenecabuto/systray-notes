# Systray Notes

A systray icon that list .txt,.md,.mkd,.rst files in a given notes folder.

It is syncronized with folder updates.

And on click opens your favorite editor.


# Install it

...


# Usage

systraynotes.py <notes path> <editor bin path>


# Example

systraynotes.py $HOME/Documents/ retext


## TODO

- tecnico
    - resolver problema de bloquear quando abre o editor ok
    - listagem com botao direito
    - resolver problema do parent thread
    - remover note

- feature
    - limitar lista
    - ordenar por ultima atualizacao
    - adicionar importancia
    - interface para listar notas (abrir no navegador de arquivos do sistema)
    - persistir estado
    - configuracao
        - tela
        - persistir
        - propagar

    - notebook por pasta ok
        - contexto e subnavegacao ok
        - por titulo no menu ok



## Needs

PyQt4

threads

