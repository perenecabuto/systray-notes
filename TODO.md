- navegacao por notebooks separada

- notificacoes
    - quando um documento eh alterado
    - quando um documento eh adicionado
    - quando um documento eh removido

- icones no menu
    - notebooks
    - sair
    - criar novo
    - voltar

- quando chama o shortcut
    - sair com esc
    - no menu
        - marca automaticamente o primeiro documento quando esta na raiz
        - marca automaticamente .. quando estiver em um bookmark

- bug
    - nao dah foco no menu quando ativado pelo shortcut
